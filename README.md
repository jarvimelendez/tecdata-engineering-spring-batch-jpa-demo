# Tecdata Engineering Spring Batch JPA Demo 

Example of loading a user information file and saving them in a database through a batch job called from an endpoint.

## CSV file path
- `src/main/resource/user.csv`

## Load CSV to DB
- `http://localhost:8080/load` - Trigger point for Spring Batch
- `http://localhost:8080/users` - Show users in DB
- `http://localhost:8080/h2-console` - H2 Console for querying the in-memory tables.

## H2 Config
- `jdbc:h2:mem:demo` - JDBC URL.
- `sa` - User.
- `password` - Password.
