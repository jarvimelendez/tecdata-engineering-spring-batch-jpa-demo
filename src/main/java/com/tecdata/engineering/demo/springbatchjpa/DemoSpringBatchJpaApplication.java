package com.tecdata.engineering.demo.springbatchjpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoSpringBatchJpaApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoSpringBatchJpaApplication.class, args);
    }

}
