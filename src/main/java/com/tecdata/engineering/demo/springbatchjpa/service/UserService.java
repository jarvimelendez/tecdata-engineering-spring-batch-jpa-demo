package com.tecdata.engineering.demo.springbatchjpa.service;

import com.tecdata.engineering.demo.springbatchjpa.model.User;
import com.tecdata.engineering.demo.springbatchjpa.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

}
