package com.tecdata.engineering.demo.springbatchjpa.config;

import com.tecdata.engineering.demo.springbatchjpa.batch.UserDBWriter;
import com.tecdata.engineering.demo.springbatchjpa.batch.UserFileReader;
import com.tecdata.engineering.demo.springbatchjpa.batch.UserProcessor;
import com.tecdata.engineering.demo.springbatchjpa.dto.UserDTO;
import com.tecdata.engineering.demo.springbatchjpa.model.User;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableBatchProcessing
public class ETLUserBatchConfig {

    public final JobBuilderFactory jobBuilderFactory;

    public final StepBuilderFactory stepBuilderFactory;

    private final UserFileReader userFileReader;

    private final UserDBWriter userDBWriter;

    private final UserProcessor userProcessor;

    @Autowired
    public ETLUserBatchConfig(JobBuilderFactory jobBuilderFactory, StepBuilderFactory stepBuilderFactory,
                              UserFileReader userFileReader, UserDBWriter userDBWriter, UserProcessor userProcessor) {
        this.jobBuilderFactory = jobBuilderFactory;
        this.stepBuilderFactory = stepBuilderFactory;
        this.userFileReader = userFileReader;
        this.userDBWriter = userDBWriter;
        this.userProcessor = userProcessor;
    }

    @Bean
    public Job createJob() {
        return jobBuilderFactory.get("ETL-users")
                .incrementer(new RunIdIncrementer())
                .flow(createStep()).end().build();
    }

    @Bean
    public Step createStep() {
        return stepBuilderFactory.get("ETL-users-step1")
                .<UserDTO, User>chunk(2)
                .reader(userFileReader)
                .writer(userDBWriter)
                .processor(userProcessor)
                .build();
    }

}
