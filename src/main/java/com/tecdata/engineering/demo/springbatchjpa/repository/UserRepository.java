package com.tecdata.engineering.demo.springbatchjpa.repository;

import com.tecdata.engineering.demo.springbatchjpa.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer> {
}
