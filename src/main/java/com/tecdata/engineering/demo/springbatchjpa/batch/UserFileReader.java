package com.tecdata.engineering.demo.springbatchjpa.batch;

import com.tecdata.engineering.demo.springbatchjpa.dto.UserDTO;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Component;

@Component
public class UserFileReader extends FlatFileItemReader<UserDTO> implements ItemReader<UserDTO> {

    public UserFileReader() {
        setResource(new FileSystemResource("src/main/resources/users.csv"));
        setLinesToSkip(1);
        setLineMapper(lineMapper());
        setName("CSV-Reader");
    }

    public LineMapper<UserDTO> lineMapper() {

        DefaultLineMapper<UserDTO> defaultLineMapper = new DefaultLineMapper<>();
        DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();

        lineTokenizer.setDelimiter(",");
        lineTokenizer.setStrict(false);
        lineTokenizer.setNames("id", "fullName", "dept", "salary");

        BeanWrapperFieldSetMapper<UserDTO> fieldSetMapper = new BeanWrapperFieldSetMapper<>();
        fieldSetMapper.setTargetType(UserDTO.class);

        defaultLineMapper.setLineTokenizer(lineTokenizer);
        defaultLineMapper.setFieldSetMapper(fieldSetMapper);

        return defaultLineMapper;
    }
}
