package com.tecdata.engineering.demo.springbatchjpa.batch;

import com.tecdata.engineering.demo.springbatchjpa.model.User;
import com.tecdata.engineering.demo.springbatchjpa.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
public class UserDBWriter implements ItemWriter<User> {

    private final UserRepository userRepository;

    @Autowired
    public UserDBWriter(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void write(List<? extends User> users) {
        log.info("Data Saved for Users: {}", users);
        userRepository.saveAll(users);
    }
}
