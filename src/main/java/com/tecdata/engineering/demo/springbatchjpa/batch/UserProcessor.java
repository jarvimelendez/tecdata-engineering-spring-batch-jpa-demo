package com.tecdata.engineering.demo.springbatchjpa.batch;

import com.tecdata.engineering.demo.springbatchjpa.dto.UserDTO;
import com.tecdata.engineering.demo.springbatchjpa.model.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Component
public class UserProcessor implements ItemProcessor<UserDTO, User> {

    private static final Map<String, String> DEPT_NAMES =
            new HashMap<>();

    public UserProcessor() {
        DEPT_NAMES.put("001", "Technology");
        DEPT_NAMES.put("002", "Operations");
        DEPT_NAMES.put("003", "Accounts");
    }

    @Override
    public User process(UserDTO userDTO) {

        final String dept = getDeptNameByCode(userDTO.getDept());
        String[] nameSplit = splitFullName(userDTO.getFullName());

        User user = new User();
        user.setId(userDTO.getId());
        user.setDept(dept);
        user.setFirstName(nameSplit[0]);
        user.setLastName(nameSplit[1]);
        user.setSalary(userDTO.getSalary());
        user.setTime(new Date());

        return user;
    }

    public String getDeptNameByCode(String deptCode) {
        String dept = DEPT_NAMES.get(deptCode);
        log.info("Converted from [{}] to [{}]", deptCode, dept);
        return dept;
    }

    public String[] splitFullName(String fullName) {
        String[] splitName = fullName.split(" ");
        if (splitName.length != 2) {
            throw new IllegalArgumentException("The user must have first and last name separated by a space");
        }
        return splitName;
    }

}
