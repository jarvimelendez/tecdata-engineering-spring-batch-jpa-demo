package com.tecdata.engineering.demo.springbatchjpa.batch;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class UserProcessorTest {

    @Test
    @DisplayName("Given department code, When department exist, Then should return department name")
    void givenDeptCodeWhenExistThenDeptName() {
        UserProcessor userProcessor = new UserProcessor();
        String deptName = userProcessor.getDeptNameByCode("001");
        Assertions.assertEquals("Technology", deptName);
    }

    @Test
    @DisplayName("Given department code, When department exist, Then should return null")
    void givenDeptCodeWhenNotExistThenDeptName() {
        UserProcessor userProcessor = new UserProcessor();
        String deptNameNull = userProcessor.getDeptNameByCode("NOT_EXIST");
        Assertions.assertNull(deptNameNull);
    }

    @Test
    @DisplayName("Given full name, When have space between, Then split in first and last name")
    void givenFullNameWheHaveSpaceBetweenThenSplit() {
        UserProcessor userProcessor = new UserProcessor();
        String[] fullNameSplit = userProcessor.splitFullName("Tammy Nordquist");
        Assertions.assertArrayEquals(new String[] {"Tammy", "Nordquist"}, fullNameSplit);
    }

    @Test
    @DisplayName("Given full name, When cannot be split in two, Then Throws IllegalArgumentException")
    void givenFullNameWhenCannotBySplitInTwoThrowsIllegalArgumentException() {
        UserProcessor userProcessor = new UserProcessor();
        Assertions.assertThrows(IllegalArgumentException.class, () -> userProcessor.splitFullName("ClaraTorres"));
    }
}
